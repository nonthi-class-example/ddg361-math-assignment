using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHurtFlash : MonoBehaviour
{
    [SerializeField] private float _flashDuration = 0.5f;
    [SerializeField] private float _flashSpeed = 10f;
    [SerializeField] private Color _flashColor = Color.white;

    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField] private string _flashStrengthKey = "_FlashStrength";
    [SerializeField] private string _flashColorKey = "_FlashColor";

    [SerializeField] private CharacterHealth _characterHealth;

    private float _flashTimer;
    private bool _flashing;

    private void OnEnable()
    {
        _characterHealth.OnHealthChanged += HandleHealthChanged;
    }

    private void OnDisable()
    {
        _characterHealth.OnHealthChanged -= HandleHealthChanged;
    }

    private void Update()
    {
        if (_flashing)
        {
            if (_flashTimer >= _flashDuration)
            {
                _flashing = false;
                _renderer.material.SetFloat(_flashStrengthKey, 0f);
                return;
            }

            _flashTimer += Time.deltaTime;
            _renderer.material.SetFloat(_flashStrengthKey, Mathf.PingPong(_flashTimer * _flashSpeed, 1f));
        }
    }

    private void HandleHealthChanged(float current, float max, float delta)
    {
        if (delta < 0f)
        {
            _flashing = true;
            _flashTimer = 0f;
            _renderer.material.SetColor(_flashColorKey, _flashColor);
        }
    }
}
