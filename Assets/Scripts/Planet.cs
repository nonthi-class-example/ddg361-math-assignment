using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    [SerializeField] private float _gravityStrength = 30f;
    [SerializeField] private PlayerController _player;

    private void FixedUpdate()
    {
        _player.SetGravity((transform.position - _player.GetCenter()).normalized * _gravityStrength);
    }
}
