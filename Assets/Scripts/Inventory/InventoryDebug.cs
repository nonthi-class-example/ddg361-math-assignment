using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InventoryDebug : MonoBehaviour
{
    [SerializeField] private InventoryScreen _inventoryScreen;
    [SerializeField] private PlayerInput _input;

    private PlayerControls _playerControls;
    private InputAction _toggleAction;

    private void Awake()
    {
        _playerControls = new PlayerControls();
        _toggleAction = _input.actions[_playerControls.HUD.ToggleInventory.name];
    }

    private void OnEnable()
    {
        _toggleAction.performed += HandleToggle;
    }

    private void OnDisable()
    {
        _toggleAction.performed -= HandleToggle;
    }

    private void HandleToggle(InputAction.CallbackContext context)
    {
        _inventoryScreen.Toggle();
    }
}
