using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatBar : MonoBehaviour
{
    [SerializeField] private RectTransform _fillRectTransform;

    public void SetStatValue(float current, float max)
    {
        Vector2 anchorMax = _fillRectTransform.anchorMax;
        anchorMax.x = Mathf.Clamp01(current / max);
        _fillRectTransform.anchorMax = anchorMax;
    }
}
