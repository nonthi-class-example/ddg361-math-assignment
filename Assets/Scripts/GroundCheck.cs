using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    [SerializeField] private Collider2D _collider;
    [SerializeField] private float _castRange = 0.02f;

    private bool _grounded;
    public bool Grounded => _grounded;

    private RaycastHit2D[] _hits = new RaycastHit2D[16];

    private void FixedUpdate()
    {
        _grounded = _collider.Cast(-transform.up, _hits, _castRange) > 0;
    }
}
