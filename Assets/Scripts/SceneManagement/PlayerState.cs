using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour, ISavable<PlayerStateSaveData>
{
    [SerializeField] private PlayerSpawnSystem _spawnSystem;

    private float _health;
    public float Health => _health;

    private bool _hasRecordedState;

    public void RecordPlayerState(PlayerCharacter pc)
    {
        if (pc == null) return;

        _health = pc.Health.CurrentHealth;

        _hasRecordedState = true;
    }

    public void ApplyRecordedState(PlayerCharacter pc)
    {
        if (!_hasRecordedState) return;

        pc.Health.SetHealth(_health);
    }

    public void ApplySaveData(PlayerStateSaveData saveData)
    {
        _health = saveData.health;
        _hasRecordedState = true;
    }

    public PlayerStateSaveData GetSaveData()
    {
        RecordPlayerState(_spawnSystem.CurrentPlayerCharacter);

        PlayerStateSaveData saveData = new PlayerStateSaveData();
        saveData.health = _health;
        return saveData;
    }
}

[System.Serializable]
public class PlayerStateSaveData
{
    public float health;
}
