using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationExit : MonoBehaviour
{
    [SerializeField] private string _nextLevelName;
    [SerializeField] private string _key;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out PlayerCharacter pc))
        {
            GameplayManager.Instance.PlayerSpawnSystem.SetSpawnKey(_key);

            AppManager.Instance.SaveSystem.RecordSaveGameData(AppManager.Instance.SceneLoader.GetCurrentLocationName());

            AppManager.Instance.SceneLoader.LoadLocation(_nextLevelName);
        }
    }
}
