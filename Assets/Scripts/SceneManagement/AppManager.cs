using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : Singleton<AppManager>
{
    [SerializeField] private SceneLoader _sceneLoader;
    public SceneLoader SceneLoader => _sceneLoader;

    [SerializeField] private SaveSystem _saveSystem;
    public SaveSystem SaveSystem => _saveSystem;
}
