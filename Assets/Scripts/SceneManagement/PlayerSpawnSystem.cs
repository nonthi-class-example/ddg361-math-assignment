using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerSpawnSystem : MonoBehaviour, ISavable<PlayerSpawnSystemSaveData>
{
    [SerializeField] private PlayerCharacter _playerPrefab;
    [SerializeField] private PlayerState _playerState;

    private string _spawnKey;

    private PlayerCharacter _pc;
    public PlayerCharacter CurrentPlayerCharacter => _pc;

    [SerializeField] private UnityEvent<PlayerCharacter> _onPlayerSpawned;

    public void SetSpawnKey(string key)
    {
        _spawnKey = key;
    }

    public void SpawnPlayer()
    {
        LocationEntrance[] entrances = FindObjectsOfType<LocationEntrance>();
        LocationEntrance entrance;
        float spawnDirection = 0f;
        Vector3 spawnLocation = transform.position;

        if (string.IsNullOrEmpty(_spawnKey))
        {
            entrance = entrances.SingleOrDefault(x => x.IsDefault);
        }
        else
        {
            entrance = entrances.SingleOrDefault(x => _spawnKey.Equals(x.Key));
        }

        if (entrance != null)
        {
            spawnLocation = entrance.transform.position;
            spawnDirection = entrance.FacingDirection;
        }

        _pc = Instantiate(_playerPrefab, spawnLocation, Quaternion.identity);

        _playerState.ApplyRecordedState(_pc);

        _pc.Controller.SetFacingDirection(spawnDirection);

        _onPlayerSpawned?.Invoke(_pc);
    }

    public void ApplySaveData(PlayerSpawnSystemSaveData saveData)
    {
        _spawnKey = saveData.spawnKey;
    }

    public PlayerSpawnSystemSaveData GetSaveData()
    {
        PlayerSpawnSystemSaveData saveData = new PlayerSpawnSystemSaveData();

        saveData.spawnKey = _spawnKey;

        return saveData;
    }
}

[System.Serializable]
public class PlayerSpawnSystemSaveData
{
    public string spawnKey;
}
