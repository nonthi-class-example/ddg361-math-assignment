using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour
{
    [SerializeField] private float _maxHealth = 10f;
    public float MaxHealth => _maxHealth;

    private float _currentHealth;
    public float CurrentHealth => _currentHealth;

    public delegate void HealthChangedDelegate(float current, float max, float delta);
    public event HealthChangedDelegate OnHealthChanged;

    private void Awake()
    {
        _currentHealth = _maxHealth;
    }

    public void TakeDamage(float damage)
    {
        float prevHealth = _currentHealth;
        _currentHealth = Mathf.Clamp(_currentHealth - damage, 0f, _maxHealth);

        OnHealthChanged?.Invoke(_currentHealth, _maxHealth, _currentHealth - prevHealth);
    }

    public void SetHealth(float value)
    {
        _currentHealth = Mathf.Clamp(value, 0f, _maxHealth);
    }
}
