using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _initialSpeed = 10f;
    [SerializeField] private float _lifeSpan = 5f;

    private Rigidbody2D _rb;

    private float _lifeTimer;

    private void Awake()
    {
        TryGetComponent(out _rb);
        _rb.useFullKinematicContacts = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        _rb.velocity = transform.right * _initialSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        _lifeTimer += Time.deltaTime;
        if (_lifeTimer >= _lifeSpan)
        {
            Despawn();
        }
    }

    private void Despawn()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Despawn();
    }
}
