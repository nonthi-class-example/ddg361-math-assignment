using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PauseScreenDebug : MonoBehaviour
{
    [SerializeField] private PauseScreen _pauseScreen;
    [SerializeField] private PlayerInput _input;

    private PlayerControls _playerControls;
    private InputAction _toggleAction;

    private void Awake()
    {
        _playerControls = new PlayerControls();
        _toggleAction = _input.actions[_playerControls.HUD.TogglePauseMenu.name];
    }

    private void OnEnable()
    {
        _toggleAction.performed += HandleToggle;
    }

    private void OnDisable()
    {
        _toggleAction.performed -= HandleToggle;
    }

    private void HandleToggle(InputAction.CallbackContext context)
    {
        _pauseScreen.Toggle();
    }
}
