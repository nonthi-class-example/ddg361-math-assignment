using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private int _slotCapacity = 10;
    public int SlotCapacity => _slotCapacity;

    public event System.Action OnInventoryUpdated = delegate { };

    private List<InventorySlot> _slots = new List<InventorySlot>();

    private void Awake()
    {
        for (int i = 0; i < _slotCapacity; i++)
        {
            _slots.Add(new InventorySlot());
        }
    }

    public void AddItemDebug(ItemSO baseItem)
    {
        AddItem(baseItem);
    }

    public bool AddItem(ItemSO baseItem)
    {
        InventorySlot firstEmptySlot = null;

        bool hasAdded = false;

        foreach (InventorySlot aSlot in _slots)
        {
            if (aSlot.IsEmpty() && firstEmptySlot == null)
            {
                firstEmptySlot = aSlot;
            }

            if (aSlot.BaseItem == baseItem && !aSlot.IsFull())
            {
                aSlot.AddCount();
                hasAdded = true;
            }
        }

        if (!hasAdded && firstEmptySlot != null)
        {
            firstEmptySlot.SetItem(baseItem, 1);
            hasAdded = true;
        }

        if (hasAdded)
        {
            OnInventoryUpdated?.Invoke();
        }

        return hasAdded;
    }

    public InventorySlot GetSlot(int index)
    {
        return _slots[index];
    }
}
