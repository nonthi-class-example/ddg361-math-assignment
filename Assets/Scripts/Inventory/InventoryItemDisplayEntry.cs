using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryItemDisplayEntry : MonoBehaviour
{
    [SerializeField] private Image _itemImage;
    [SerializeField] private TextMeshProUGUI _itemCountText;

    private InventorySlot _slot;
    private InventoryScreen _screen;

    public void SetInventoryScreen(InventoryScreen screen)
    {
        _screen = screen;
    }

    public void SetSlot(InventorySlot slot)
    {
        _slot = slot;

        if (slot.IsEmpty())
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
            _itemImage.sprite = slot.BaseItem.ItemSprite;
            _itemCountText.text = "" + slot.StackCount;
        }
    }

    public void HandleSelected()
    {
        _screen.ShowItemInfo(_slot);
    }
}
