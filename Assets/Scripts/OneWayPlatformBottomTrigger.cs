using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPlatformBottomTrigger : MonoBehaviour
{
    [SerializeField] private Collider2D _platformCollider;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Ignore");
        Physics2D.IgnoreCollision(_platformCollider, collision, true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("Collide");
        Physics2D.IgnoreCollision(_platformCollider, collision, false);
    }
}
