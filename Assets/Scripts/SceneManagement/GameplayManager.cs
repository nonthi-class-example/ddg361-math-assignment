using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : Singleton<GameplayManager>
{
    [SerializeField] private PlayerSpawnSystem _playerSpawnSystem;
    public PlayerSpawnSystem PlayerSpawnSystem => _playerSpawnSystem;

    [SerializeField] private PlayerState _playerState;
    public PlayerState PlayerState => _playerState;
}
