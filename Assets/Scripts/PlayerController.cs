using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private PlayerInput _input;

    [SerializeField] private float _moveSpeed = 5f;
    [SerializeField] private float _jumpPower = 10f;

    [SerializeField]
    private Vector2 _initialGravity = new Vector2(0, -30f);

    [SerializeField]
    private GroundCheck _groundCheck;

    [Header("Attack")]
    [SerializeField]
    private Bullet _bulletPrefab;
    [SerializeField]
    private Transform _firePoint;
    [SerializeField]
    private float _attackCooldown = 0.25f;

    private Rigidbody2D _rb;
    private CapsuleCollider2D _capsule;

    private Vector2 _gravity;
    private Vector3 _screenCamForward = Vector3.forward;

    private float _attackCooldownCounter;

    private PlayerControls _playerControls;
    private InputAction _moveAction;
    private InputAction _jumpAction;
    private InputAction _fireAction;

    public Vector3 SideScrollingRight
    {
        get
        {
            return Vector3.Cross(transform.up, _screenCamForward);
        }
    }

    private void Awake()
    {
        TryGetComponent(out _rb);
        TryGetComponent(out _capsule);
        _gravity = _initialGravity;

        _playerControls = new PlayerControls();
        _moveAction = _input.actions[_playerControls.Main.Move.name];
        _jumpAction = _input.actions[_playerControls.Main.Jump.name];
        _fireAction = _input.actions[_playerControls.Main.Fire.name];
    }

    private void OnEnable()
    {
        _jumpAction.performed += HandleJump;
        _fireAction.performed += HandleFire;
    }

    private void OnDisable()
    {
        _jumpAction.performed -= HandleJump;
        _fireAction.performed -= HandleFire;
    }

    private void Update()
    {
        _attackCooldownCounter = Mathf.MoveTowards(_attackCooldownCounter, 0f, Time.deltaTime);
    }

    private void FixedUpdate()
    {
        AlignWithGravity();

        Move();

        _rb.AddForce(_rb.mass * _gravity);
    }

    private void Move()
    {
        float horzInput = _moveAction.ReadValue<Vector2>().x;

        if (Mathf.Approximately(horzInput, 0f))
        {
            horzInput = 0f;
        }
        else
        {
            horzInput = Mathf.Sign(horzInput);
        }

        SetFacingDirection(horzInput);

        _rb.velocity = (horzInput * _moveSpeed * SideScrollingRight) + Vector3.Project(_rb.velocity, transform.up);
    }

    public void SetFacingDirection(float dir)
    {
        if (Mathf.Approximately(dir, 0f)) return;
        transform.rotation = Quaternion.LookRotation(dir * _screenCamForward, -_gravity);
    }

    public Vector3 GetCenter()
    {
        return _capsule.bounds.center;
    }

    private void AlignWithGravity()
    {
        Vector3 center = GetCenter();

        Vector3 centerToPos = transform.position - center;

        Quaternion rotationDelta = Quaternion.FromToRotation(transform.up, -_gravity);

        Vector3 target = center + rotationDelta * centerToPos;

        Quaternion newRotation = Quaternion.LookRotation(transform.forward, -_gravity);

        transform.SetPositionAndRotation(target, newRotation);
    }

    public void SetGravity(Vector2 gravity)
    {
        _gravity = gravity;
    }

    private void HandleJump(InputAction.CallbackContext context)
    {
        if (!_groundCheck.Grounded) return;

        _rb.velocity = Vector3.ProjectOnPlane(_rb.velocity, transform.up) + (_jumpPower * transform.up);
    }

    private void HandleFire(InputAction.CallbackContext obj)
    {
        if (_attackCooldownCounter > 0f) return;

        Instantiate(_bulletPrefab, _firePoint.position, _firePoint.rotation);
    }
}
