using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBuffVisual : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField] private float _buffStrength;
    [SerializeField] private Color _buffColor = Color.green;
    [SerializeField] private float _buffDirection = -1f;

    [SerializeField] private string _buffStrengthKey = "_BuffStrength";
    [SerializeField] private string _buffColorKey = "_BuffColor";
    [SerializeField] private string _buffDirectionKey = "_BuffDirection";

    // Update is called once per frame
    void Update()
    {
        _renderer.material.SetFloat(_buffStrengthKey, _buffStrength);
        _renderer.material.SetColor(_buffColorKey, _buffColor);
        _renderer.material.SetFloat(_buffDirectionKey, _buffDirection);
    }
}
