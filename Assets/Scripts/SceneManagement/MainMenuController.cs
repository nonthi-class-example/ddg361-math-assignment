using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private string _firstLocationName = "FirstScene";
    [SerializeField] private string _firstLocationSpawnKey;

    [SerializeField] private GameObject _continueButton;

    private void Start()
    {
        if (AppManager.Instance != null)
        {
            _continueButton.gameObject.SetActive(AppManager.Instance.SaveSystem.HasSaveGameInSlot(0));
        }
    }

    public void StartGame()
    {
        AppManager.Instance.SaveSystem.ClearSavedGameData();
        AppManager.Instance.SceneLoader.LoadLocation(_firstLocationName);
    }

    public void ContinueGame()
    {
        AppManager.Instance.SaveSystem.LoadGameFromSlot(0);
    }
}
