using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private string _gameplaySceneName = "Gameplay";

    [SerializeField] private SaveSystem _saveSystem;

    private bool _isLoading;

    private string _currentLocationName;

    public string GetCurrentLocationName()
    {
        return _currentLocationName;
    }

    public void LoadMenu(string menuName)
    {
        if (_isLoading) return;

        StartCoroutine(LoadMenuCoroutine(menuName));
    }

    private IEnumerator LoadMenuCoroutine(string menuName)
    {
        _isLoading = true;

        AsyncOperation unloadOp = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        while (!unloadOp.isDone)
        {
            yield return null;
        }

        Scene gameplayScene = SceneManager.GetSceneByName(_gameplaySceneName);
        if (gameplayScene.isLoaded)
        {
            AsyncOperation gameplayLoadOp = SceneManager.UnloadSceneAsync(gameplayScene);
            while (!gameplayLoadOp.isDone)
            {
                yield return null;
            }
        }

        AsyncOperation loadOp = SceneManager.LoadSceneAsync(menuName, LoadSceneMode.Additive);
        loadOp.allowSceneActivation = true;
        while (!loadOp.isDone)
        {
            yield return null;
        }

        Scene menuScene = SceneManager.GetSceneByName(menuName);
        SceneManager.SetActiveScene(menuScene);

        _isLoading = false;
    }

    public void LoadLocation(string levelName)
    {
        if (_isLoading) return;

        StartCoroutine(LoadLevelCoroutine(levelName));
    }

    private IEnumerator LoadLevelCoroutine(string levelName)
    {
        _isLoading = true;

        AsyncOperation unloadOp = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        while (!unloadOp.isDone)
        {
            yield return null;
        }

        Scene gameplayScene = SceneManager.GetSceneByName(_gameplaySceneName);
        if (!gameplayScene.isLoaded)
        {
            AsyncOperation gameplayLoadOp = SceneManager.LoadSceneAsync(_gameplaySceneName, LoadSceneMode.Additive);
            while (!gameplayLoadOp.isDone)
            {
                yield return null;
            }
        }

        AsyncOperation loadOp = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
        loadOp.allowSceneActivation = true;
        while (!loadOp.isDone)
        {
            yield return null;
        }

        Scene locationScene = SceneManager.GetSceneByName(levelName);
        _currentLocationName = levelName;
        SceneManager.SetActiveScene(locationScene);

        OnGameplayLevelLoaded();

        _isLoading = false;
    }

    public void OnGameplayLevelLoaded()
    {
        _saveSystem.ApplySaveGameData();
        GameplayManager.Instance.PlayerSpawnSystem.SpawnPlayer();
    }
}
