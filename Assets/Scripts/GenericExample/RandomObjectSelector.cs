using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RandomObjectSelector
{
    [SerializeField] private List<RandomObjectEntry> _entries = new List<RandomObjectEntry>();

    public RandomObjectEntry GetRandom()
    {
        float total = 0;

        foreach (RandomObjectEntry entry in _entries)
        {
            total += entry.Weight;
        }

        float randomPoint = Random.value * total;

        for (int i = 0; i < _entries.Count; i++)
        {
            RandomObjectEntry entry = _entries[i];
            if (randomPoint < entry.Weight)
            {
                return entry;
            }
            else
            {
                randomPoint -= entry.Weight;
            }
        }
        return _entries[_entries.Count - 1];
    }
}

public class RandomObjectEntry
{
    [SerializeField]
    private GameObject _prefab;
    public GameObject Prefab => _prefab;

    [SerializeField]
    private float _weight;
    public float Weight => _weight;
}
