using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    private PlayerController _controller;
    public PlayerController Controller => _controller;

    private CharacterHealth _health;
    public CharacterHealth Health => _health;

    private void Awake()
    {
        TryGetComponent(out _controller);
        TryGetComponent(out _health);
    }
}
