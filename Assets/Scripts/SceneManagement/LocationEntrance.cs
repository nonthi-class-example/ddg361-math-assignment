using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationEntrance : MonoBehaviour
{
    [SerializeField] private string _key;
    public string Key => _key;

    [SerializeField] private float _facingDirection;
    public float FacingDirection => _facingDirection;

    [SerializeField] private bool _isDefault;
    public bool IsDefault => _isDefault;
}
