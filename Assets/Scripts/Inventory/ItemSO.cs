using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Contents/Item")]
public class ItemSO : ScriptableObject
{
    [SerializeField] private string _displayName;
    public string DisplayName => _displayName;

    [TextArea]
    [SerializeField] private string _description;
    public string Description => _description;

    [SerializeField] private Sprite _itemSprite;
    public Sprite ItemSprite => _itemSprite;

    [SerializeField] private int _stackCapacity = 99;
    public int StackCapacity => _stackCapacity;
}
