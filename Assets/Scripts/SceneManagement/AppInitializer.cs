using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppInitializer : MonoBehaviour
{
    [SerializeField] private string _managerSceneName = "Manager";
    [SerializeField] private string _menuSceneName = "MainMenu";

    private void Awake()
    {
        StartCoroutine(LoadManagerScenes());
    }

    private IEnumerator LoadManagerScenes()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;

        Scene managerScene = SceneManager.GetSceneByName(_managerSceneName);
        if (!managerScene.isLoaded)
        {
            SceneManager.LoadScene(_managerSceneName, LoadSceneMode.Additive);

            yield return null;

            AppManager.Instance.SceneLoader.LoadMenu(_menuSceneName);
        }

        yield break;
    }
}
