using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScreen : MonoBehaviour
{
    [SerializeField] private string _mainMenuName = "MainMenu";
    [SerializeField] private GameObject _rootObject;

    private bool _paused;

    public void Toggle()
    {
        if (_paused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    public void Pause()
    {
        _rootObject.SetActive(true);
        Time.timeScale = 0f;

        _paused = true;
    }

    public void Resume()
    {
        _rootObject.SetActive(false);
        Time.timeScale = 1f;

        _paused = false;
    }

    public void Quit()
    {
        Resume();

        AppManager.Instance.SaveSystem.SaveGameToCurrentSlot(
            AppManager.Instance.SceneLoader.GetCurrentLocationName(),
            () =>
            {
                AppManager.Instance.SceneLoader.LoadMenu(_mainMenuName);
            }
            );
    }
}
