using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EditorColdStartup : MonoBehaviour
{
    [SerializeField] private bool _isMenu;

    [SerializeField] private string _managerSceneName = "Manager";

    private void Awake()
    {
        StartCoroutine(LoadManagerScenes());
    }

    private IEnumerator LoadManagerScenes()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;

        Scene managerScene = SceneManager.GetSceneByName(_managerSceneName);
        if (!managerScene.isLoaded)
        {
            SceneManager.LoadScene(_managerSceneName, LoadSceneMode.Additive);

            yield return null;

            if (_isMenu)
            {
                AppManager.Instance.SceneLoader.LoadMenu(currentSceneName);
            }
            else
            {
                AppManager.Instance.SceneLoader.LoadLocation(currentSceneName);
            }
        }

        yield break;
    }
}
