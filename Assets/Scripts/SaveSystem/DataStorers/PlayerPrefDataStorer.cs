using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsDataStorer : IDataStorer
{
    public bool HasData(string fileName)
    {
        return PlayerPrefs.HasKey(fileName);
    }

    public IEnumerator StoreData(string fileName, SaveSystem saveSystem, string data)
    {
        PlayerPrefs.SetString(fileName, data);
        PlayerPrefs.Save();

        yield break;
    }

    public string RetrieveData(string fileName)
    {
        string s = PlayerPrefs.GetString(fileName);
        return HasData(fileName) ? s : string.Empty;
    }
}
