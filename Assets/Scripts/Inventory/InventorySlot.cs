using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySlot
{
    private ItemSO _baseItem;
    public ItemSO BaseItem => _baseItem;

    private int _stackCount;
    public int StackCount => _stackCount;

    public bool IsEmpty()
    {
        return _baseItem == null || _stackCount <= 0;
    }

    public bool IsFull()
    {
        if (_baseItem == null) return false;

        return _stackCount >= _baseItem.StackCapacity;
    }

    public void SetItem(ItemSO baseItem, int count)
    {
        _baseItem = baseItem;
        _stackCount = count;
    }

    public void AddCount()
    {
        _stackCount++;
    }

    public void Clear()
    {
        SetItem(null, 0);
    }
}
