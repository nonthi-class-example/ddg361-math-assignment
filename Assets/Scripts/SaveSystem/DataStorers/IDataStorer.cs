using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDataStorer
{
    bool HasData(string fileName);
    IEnumerator StoreData(string fileName, SaveSystem saveSystem, string data);
    string RetrieveData(string fileName);
}
