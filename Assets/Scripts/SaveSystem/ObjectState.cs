using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectState : MonoBehaviour, ISavable<ObjectStateSaveData>
{
    [SerializeField] private GameObject _refObject;

    public void ApplySaveData(ObjectStateSaveData saveData)
    {
        if (saveData.isDestroyed)
        {
            Destroy(_refObject);
            return;
        }

        _refObject.transform.SetPositionAndRotation(saveData.position, saveData.rotation);
    }

    public ObjectStateSaveData GetSaveData()
    {
        ObjectStateSaveData saveData = new ObjectStateSaveData();

        if (_refObject == null)
        {
            saveData.isDestroyed = true;
        }
        else
        {
            saveData.isDestroyed = false;
            saveData.position = _refObject.transform.position;
            saveData.rotation = _refObject.transform.rotation;
        }

        return saveData;
    }
}

public class ObjectStateSaveData
{
    public Vector3 position;
    public Quaternion rotation;
    public bool isDestroyed;
}
