using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorShifter : MonoBehaviour
{
    private SpriteRenderer _renderer;

    private void Awake()
    {
        TryGetComponent(out _renderer);
    }

    public void SwitchColor()
    {
        _renderer.color = Random.ColorHSV();
    }
}
