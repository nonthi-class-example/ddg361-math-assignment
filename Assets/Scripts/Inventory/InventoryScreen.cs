using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryScreen : MonoBehaviour
{
    [SerializeField] private GameObject _rootObject;
    [SerializeField] private Inventory _inventory;
    [SerializeField] private RectTransform _itemGridTransform;
    [SerializeField] private InventoryItemDisplayEntry _displayEntryPrefab;

    [SerializeField] private Image _itemImage;
    [SerializeField] private TextMeshProUGUI _itemNameText;
    [SerializeField] private TextMeshProUGUI _itemDescriptionText;

    private List<InventoryItemDisplayEntry> _displayEntries = new List<InventoryItemDisplayEntry>();

    private void Awake()
    {
        for (int i = 0; i < _inventory.SlotCapacity; i++)
        {
            InventoryItemDisplayEntry newDisplayEntry = Instantiate(_displayEntryPrefab, _itemGridTransform);
            newDisplayEntry.SetInventoryScreen(this);
            _displayEntries.Add(newDisplayEntry);
        }
    }

    private void OnEnable()
    {
        _inventory.OnInventoryUpdated += UpdateDisplay;
    }

    private void OnDisable()
    {
        _inventory.OnInventoryUpdated -= UpdateDisplay;
    }

    private void UpdateDisplay()
    {
        for (int i = 0; i < _displayEntries.Count; i++)
        {
            InventoryItemDisplayEntry aDisplayEntry = _displayEntries[i];
            aDisplayEntry.SetSlot(_inventory.GetSlot(i));
        }
    }

    public void Toggle()
    {
        UpdateDisplay();
        _rootObject.SetActive(!_rootObject.activeSelf);

        if (_rootObject.activeSelf)
        {
            ClearItemInfo();
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(_displayEntries[0].gameObject);
        }
    }

    private void ClearItemInfo()
    {
        _itemImage.sprite = null;
        _itemImage.color = Color.clear;
        _itemNameText.text = "";
        _itemDescriptionText.text = "";
    }

    public void ShowItemInfo(InventorySlot slot)
    {
        _itemImage.sprite = slot.BaseItem.ItemSprite;
        _itemImage.color = Color.white;
        _itemNameText.text = slot.BaseItem.DisplayName;
        _itemDescriptionText.text = slot.BaseItem.Description;
    }
}
