using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthDisplayHook : MonoBehaviour
{
    [SerializeField] private StatBar _healthBar;

    private CharacterHealth _playerHealthComp;

    public void SetPlayer(PlayerCharacter pc)
    {
        _playerHealthComp = pc.Health;
        _healthBar.SetStatValue(_playerHealthComp.CurrentHealth, _playerHealthComp.MaxHealth);

        _playerHealthComp.OnHealthChanged += HandleHealthChanged;
    }

    private void HandleHealthChanged(float current, float max, float delta)
    {
        _healthBar.SetStatValue(current, max);
    }
}
